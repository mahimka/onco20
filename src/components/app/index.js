import React, { Fragment, Suspense, lazy } from 'react';
import html from 'react-inner-html';

import { gsap, EasePack } from 'gsap/all'

// const S0 = lazy(() => import(`../slide0`));
// const S1 = lazy(() => import(`../slide1`));
// const S2 = lazy(() => import(`../slide2`));
// const S3 = lazy(() => import(`../slide3`));
// const S4 = lazy(() => import(`../slide4`));
// const S5 = lazy(() => import(`../slide5`));
// const S6 = lazy(() => import(`../slide6`));
// const S7 = lazy(() => import(`../slide7`));
// const S8 = lazy(() => import(`../slide8`));
// const S9 = lazy(() => import(`../slide9`));
// const S10 = lazy(() => import(`../slide10`));
// const S11 = lazy(() => import(`../slide11`));
// const S12 = lazy(() => import(`../slide12`));
// const S13 = lazy(() => import(`../slide13`));
// const S14 = lazy(() => import(`../slide14`));

import S0 from '../slide0';
import S1 from '../slide1';
import S2 from '../slide2';
import S3 from '../slide3';
import S4 from '../slide4';
import S5 from '../slide5';
import S6 from '../slide6';
import S7 from '../slide7';
import S8 from '../slide8';
import S9 from '../slide9';
import S10 from '../slide10';
import S11 from '../slide11';
import S12 from '../slide12';
import S13 from '../slide13';
import S14 from '../slide14';

import Bg from '../../assets/bg.svg';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }


  componentDidMount() {
    this.s0 = document.querySelector(`.bg-blurred[data-id="0"]`);
    this.s1 = document.querySelector(`.bg-blurred[data-id="1"]`);
    this.s2 = document.querySelector(`.bg-blurred[data-id="2"]`);

    gsap.killTweensOf([this.s0,this.s1,this.s2]);

    gsap.set(this.s0, {scale: 0.8, transformOrigin: `50% 50%`});
    gsap.to(this.s0, {scale: 2.2, ease: "power0.out", duration:3, yoyo:true, repeat:-1})

    gsap.set(this.s1, {scale: 0.8, transformOrigin: `50% 50%`});
    gsap.to(this.s1, {scale: 2.2, ease: "power1.out", duration:4, yoyo:true, repeat:-1})

    gsap.set(this.s2, {scale: 0.8, transformOrigin: `50% 50%`});
    gsap.to(this.s2, {scale: 2.2, ease: "power0.out", duration:5, yoyo:true, repeat:-1})
  }

  render() {
    const data = this.props.data;

    return (
      <Fragment>
        <div className={`mia-fixed-back`} />
        <Bg className={`mia-fixed-back-svg`} />
        <div className={`mia-infographic`}>
          
          <Suspense fallback={``}>
            <S0 data={data.sections[0]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S1 data={data.sections[1]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S2 data={data.sections[2]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S3 data={data.sections[3]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S4 data={data.sections[4]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S5 data={data.sections[5]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S6 data={data.sections[6]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S7 data={data.sections[7]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S8 data={data.sections[8]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S9 data={data.sections[9]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S10 data={data.sections[10]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S11 data={data.sections[11]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S12 data={data.sections[12]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S13 data={data.sections[13]}/>
          </Suspense>

          <Suspense fallback={``}>
            <S14 data={data.sections[14]}/>
          </Suspense>
        </div>
      </Fragment>
    );
  }
}
