import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'

export default class Slide1 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    this.els = [
      document.querySelector(`.slide1-anim0`),
      document.querySelector(`.slide1-anim1`),

    ]
    gsap.set(this.els[0], {opacity:0, x:10});
    gsap.set(this.els[1], {opacity:0, scale:0.5});


  }

  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-2'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }


  enter0(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.set(this.els[0], {opacity:0, x:10});
    gsap.set(this.els[1], {opacity:0, scale:0.5, transformOrigin: `50% 50%`});
    gsap.to(this.els[0], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els[1], {opacity: 1, scale:1, ease: "back.out(2)", duration:1})
  }
  leave0(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.to(this.els[0], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els[1], {opacity: 0, ease: "power2.in", duration:0.5})
  }



  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section`} data-id={`1`}>

          <div className={`row`}>
            <div className={`col-xs-12 col-lg-2 p-45 pt-xs-only-0 answer-col no-top-border d-none d-lg-block`}>
            </div>
            <div className={`col-xs-12 col-lg-10 p-45 pb-0 main-col no-top-border big-margin`}>
              
              <div className={`h3 mb-45 mt-45`}>{data.text[0]}</div>
              <div className={`h3 mb-45`}>{data.text[1]}</div>

              <div className={`fact position-relative`}>
                <div className={`fact-group slide1-anim1`}>
                  <div className={`fact-line`} />
                  <div className={`fact-dot`} />
                </div>
                <div className={`h3 text-primary ml-45`}>
                  <Waypoint onEnter={(props) => {this.enter0(props)}} onLeave={(props) => {this.leave0(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
                    <div className={`slide1-anim0`}>{data.fact}</div>
                  </Waypoint>
                </div>
              </div>
            </div>
          </div>

        </section>
      </Waypoint>
    );
  }
}