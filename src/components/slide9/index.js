import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { DrawSVGPlugin } from "../../vendor/DrawSVGPlugin.js";
import { EasePack } from 'gsap/all'

import Ill from '../../assets/ill6.svg';

import { connect, post, event } from '../../vendor/gtm.js'

export default class Slide9 extends React.Component {
  constructor(props) {
    super(props);
    gsap.registerPlugin(DrawSVGPlugin);
    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    this.els = [
      document.querySelector(`.slide9-anim9`),
      document.querySelector(`.slide9-anim10`),
      document.querySelector(`.slide9-anim11`)
    ]
    gsap.set(this.els[0], {opacity:0, x:-10});
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[2], {opacity:0, x:10});
    this.ill = document.querySelectorAll(`.slide9-anim8 > path`)
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});

    this.els0 = [
      document.querySelector(`.slide9-anim0`),
      document.querySelector(`.slide9-anim1`),
      document.querySelector(`.slide9-anim2`),
      document.querySelector(`.slide9-anim3`)
    ]
    this.els1 = [
      document.querySelector(`.slide9-anim0-1`),
      document.querySelector(`.slide9-anim1-1`),
      document.querySelector(`.slide9-anim2-1`),
      document.querySelector(`.slide9-anim3-1`)
    ] 
    gsap.set(this.els0, {opacity:0, x:10});
    gsap.set(this.els1, {opacity:0});

    this.horline = document.querySelector(`.hor-line9-anim`)
    gsap.set(this.horline, {scaleX:0, transformOrigin: `0% 50%`});
  }

  enterLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.set(this.horline, {opacity:0, scaleX:0, transformOrigin: `0% 50%`});
    gsap.to(this.horline, {opacity: 1, scale:1, ease: "power2.out", duration:1})
  }
  leaveLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.to(this.horline, {opacity: 0, ease: "power2.in", duration:0.5})
  }

  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-10'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }

  enterAll(id){
    gsap.killTweensOf([this.els0[id], this.els1[id]]);
    gsap.set(this.els0[id], {opacity:0, x:10});
    gsap.set(this.els1[id], {opacity:0});
    gsap.to(this.els0[id], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els1[id], {opacity: 1, ease: "power2.out", duration:1})
  }
  leaveAll(id){
    gsap.killTweensOf([this.els0[id], this.els1[id]]);
    gsap.to(this.els0[id], {opacity: 0, ease: "power2.out", duration:0.5})
    gsap.to(this.els1[id], {opacity: 0, ease: "power2.out", duration:0.5})
  }

  enter4(props){
    gsap.killTweensOf([this.ill]);
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});
    gsap.to(this.ill, {opacity: 1, drawSVG:`0% 100%`, ease: "power2.out", duration:1, stagger: 0.025})
  }
  leave4(props){
    gsap.killTweensOf([this.ill]);
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});
  }

  enter5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[0], {opacity:0, x:-10, transformOrigin: `50% 50%`});
    gsap.to(this.els[1], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els[0], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.to(this.els[1], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els[0], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.set(this.els[2], {opacity:0, x:10});
    gsap.to(this.els[2], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.to(this.els[2], {opacity: 0, ease: "power2.in", duration:0.5})
  }




  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section`} data-id={`9`}>

          <div className={`row`}>
            <Waypoint onEnter={(props) => {this.enter5(props)}} onLeave={(props) => {this.leave5(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
            <div className={`col-12 col-lg-2 p-45 d-flex justify-content-sx-start justify-content-lg-end align-items-end question-col`}>
              <div className={`display-2 manrope-light text-secondary slide9-anim9`}>{`08`}</div>
              <div className={`manrope-light pt-3 text-primary vertical-text slide9-anim10`}>{`вопрос`}</div>
            </div>
            </Waypoint>
            <div className={`col-12 col-lg-10 p-45 pt-xs-only-0 h1 manrope-bold text-uppercase text-primary`}>{data.h1}</div>
          </div>
          <Waypoint onEnter={(props) => {this.enterLine(props)}} onLeave={(props) => {this.leaveLine(props)}} bottomOffset={"10%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div className={`row no-gutters`}>
             <div className={`col-12 col-lg-9`}>
              <div className={`hor-line hor-line9-anim`} />
            </div>
          </div>
          </Waypoint>
          <div className={`row`}>
            <div className={`col-12 col-lg-2 p-45 pt-xs-only-0 d-flex justify-content-end align-items-start answer-col`}>
              <Waypoint onEnter={(props) => {this.enter6(props)}} onLeave={(props) => {this.leave6(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`manrope-light pt-0 pt-sm-3 text-primary vertical-sm-text slide9-anim11`}>{`ответ`}</div>
              </Waypoint>
            </div>
            <div className={`col-12 col-sm-8 col-lg-7 p-45 main-col`}>
              <div className={`h4 mb-45`} dangerouslySetInnerHTML={{__html: data.text[0] }} />
              <div className={`h4`} dangerouslySetInnerHTML={{__html: data.text[1] }} />
              <div className={`h4 mb-45`} dangerouslySetInnerHTML={{__html: data.text[2] }} />
              
              <Waypoint onEnter={(props) => {this.enterAll(0)}} onLeave={(props) => {this.leaveAll(0)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 text-danger mb-1 slide9-anim0`} dangerouslySetInnerHTML={{__html: data.text[3] }} />
              <div className={`h4 mb-45 slide9-anim0-1`} dangerouslySetInnerHTML={{__html: data.text[4] }} />
              </div>
              </Waypoint>

              <Waypoint onEnter={(props) => {this.enterAll(1)}} onLeave={(props) => {this.leaveAll(1)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 text-danger mb-1 slide9-anim1`} dangerouslySetInnerHTML={{__html: data.text[5] }} />
              <div className={`h4 mb-45 slide9-anim1-1`} dangerouslySetInnerHTML={{__html: data.text[6] }} />
              </div>
              </Waypoint>

              <Waypoint onEnter={(props) => {this.enterAll(2)}} onLeave={(props) => {this.leaveAll(2)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 text-danger mb-1 slide9-anim2`} dangerouslySetInnerHTML={{__html: data.text[7] }} />
              <div className={`h4 mb-45 slide9-anim2-1`} dangerouslySetInnerHTML={{__html: data.text[8] }} />
              </div>
              </Waypoint>

              <Waypoint onEnter={(props) => {this.enterAll(3)}} onLeave={(props) => {this.leaveAll(3)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 text-danger mb-1 slide9-anim3`} dangerouslySetInnerHTML={{__html: data.text[9] }} />
              <div className={`h4 mb-45 slide9-anim3-1`} dangerouslySetInnerHTML={{__html: data.text[10] }} />
              </div>
              </Waypoint>

              <div className={`h4 mb-45 withbig`} dangerouslySetInnerHTML={{__html: data.text[11] }} />
              <div className={`h4 mb-45 big-margin`} dangerouslySetInnerHTML={{__html: data.text[12] }} />
            </div>
            

            <Waypoint onEnter={(props) => {this.enter4(props)}} onLeave={(props) => {this.leave4(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
            <div className={`col-12 col-sm-4 col-lg-3 p-45 pt-0`}>
              <div className={`bold-line mb-45 d-none d-lg-block`} />
              <Ill className={`illustration slide9-anim8`} />
            </div>
            </Waypoint>


{/*
            <div className={`col-xs-12 col-sm-3 p-45 pt-0`}>
              <div className={`bold-line mb-45 d-none d-sm-block`} />
              <div className={`h6 manrope-bold text-primary mb-45`}>{data.info[0]}</div>

              <div className={`mb-45`}>
                <div className={`display-2s text-primary manrope-light value`}>{data.values[0].value}</div>
                <div className={`h6 text-right`}>{data.values[0].text}</div>
              </div>
              <div className={`mb-45`}>
                <div className={`display-2s text-primary manrope-light value`}>{data.values[1].value}</div>
                <div className={`h6 text-right`}>{data.values[1].text}</div>
              </div>
              <div className={`mb-45`}>
                <div className={`display-2s text-primary manrope-light value`}>{data.values[2].value}</div>
                <div className={`h6 text-right`}>{data.values[2].text}</div>
              </div>
              <div className={`mb-45`}>
                <div className={`display-2s text-primary manrope-light value`}>{data.values[2].value}</div>
                <div className={`h6 text-right`}>{data.values[3].text}</div>
              </div>

              <div className={`bold-line mb-45 d-none d-sm-block`} />
              <div className={`h6 manrope-bold text-primary`}>{data.info[1]}</div>
            </div>
*/}


          </div>

        </section>
      </Waypoint>
    );
  }
}
