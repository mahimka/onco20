import React,  { Fragment } from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'

export default class Slide3 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    this.els = [
      document.querySelector(`.slide3-anim9`),
      document.querySelector(`.slide3-anim10`),
      document.querySelector(`.slide3-anim11`),
    ]
    this.els0 = [
      document.querySelector(`.slide3-anim0`),
      document.querySelector(`.slide3-anim1`),
      document.querySelector(`.slide3-anim2`),
      document.querySelector(`.slide3-anim3`),
      document.querySelector(`.slide3-anim4`)
    ]
    this.els1 = [
      document.querySelector(`.slide3-anim0-1`),
      document.querySelector(`.slide3-anim1-1`),
      document.querySelector(`.slide3-anim2-1`),
      document.querySelector(`.slide3-anim3-1`),
      document.querySelector(`.slide3-anim4-1`)
    ]    
    gsap.set(this.els0, {opacity:0, x:10});
    gsap.set(this.els1, {opacity:0});

    gsap.set(this.els[0], {opacity:0, x:-10});
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[2], {opacity:0, x:10});
    this.horline = document.querySelector(`.hor-line3-anim`)
    gsap.set(this.horline, {scaleX:0, transformOrigin: `0% 50%`});
  }

  enterLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.set(this.horline, {opacity:0, scaleX:0, transformOrigin: `0% 50%`});
    gsap.to(this.horline, {opacity: 1, scale:1, ease: "power2.out", duration:1})
  }
  leaveLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.to(this.horline, {opacity: 0, ease: "power2.in", duration:0.5})
  }
  
  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-4'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }

  enterAll(id){
    gsap.killTweensOf([this.els0[id], this.els1[id]]);
    gsap.set(this.els0[id], {opacity:0, x:10});
    gsap.set(this.els1[id], {opacity:0});
    gsap.to(this.els0[id], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els1[id], {opacity: 1, ease: "power2.out", duration:1})
  }
  leaveAll(id){
    gsap.killTweensOf([this.els0[id], this.els1[id]]);
    gsap.to(this.els0[id], {opacity: 0, ease: "power2.out", duration:0.5})
    gsap.to(this.els1[id], {opacity: 0, ease: "power2.out", duration:0.5})
  }






  enter5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[0], {opacity:0, x:-10, transformOrigin: `50% 50%`});
    gsap.to(this.els[1], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els[0], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.to(this.els[1], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els[0], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.set(this.els[2], {opacity:0, x:10});
    gsap.to(this.els[2], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.to(this.els[2], {opacity: 0, ease: "power2.in", duration:0.5})
  }



  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section`} data-id={`3`}>

          <div className={`row`}>
            <Waypoint onEnter={(props) => {this.enter5(props)}} onLeave={(props) => {this.leave5(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
            <div className={`col-12 col-lg-2 p-45 d-flex justify-content-sx-start justify-content-lg-end align-items-end question-col`}>
              <div className={`display-2 manrope-light text-secondary slide3-anim9`}>{`02`}</div>
              <div className={`manrope-light pt-3 text-primary vertical-text slide3-anim10`}>{`вопрос`}</div>
            </div>
            </Waypoint>
            <div className={`col-12 col-lg-10 p-45 pt-xs-only-0 h1 manrope-bold text-uppercase text-primary`}>{data.h1}</div>
          </div>

          <Waypoint onEnter={(props) => {this.enterLine(props)}} onLeave={(props) => {this.leaveLine(props)}} bottomOffset={"10%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div className={`row no-gutters`}>
            <div className={`col-12 col-lg-9`}>
              <div className={`hor-line hor-line3-anim`} />
            </div>
          </div>
          </Waypoint>

          <div className={`row`}>
            <div className={`col-12 col-lg-2 p-45 pt-xs-only-0 d-flex justify-content-end align-items-start answer-col`}>
              <Waypoint onEnter={(props) => {this.enter6(props)}} onLeave={(props) => {this.leave6(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`manrope-light pt-0 pt-sm-3 text-primary vertical-sm-text slide3-anim11`}>{`ответ`}</div>
              </Waypoint>
            </div>
            <div className={`col-12 col-sm-8 col-lg-7 p-45 main-col`}>
              <div className={`h4 mb-45`}>{data.text[0]}</div>
              <Waypoint onEnter={(props) => {this.enterAll(0)}} onLeave={(props) => {this.leaveAll(0)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 mb-1 text-danger slide3-anim0`}>{data.text[1]}</div>
              <div className={`h4 mb-45 slide3-anim0-1`}>{data.text[2]}</div>
              </div>
              </Waypoint>
              <Waypoint onEnter={(props) => {this.enterAll(1)}} onLeave={(props) => {this.leaveAll(1)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 mb-1 text-danger withsmall slide3-anim1`} dangerouslySetInnerHTML={{__html: data.text[3] }} />
              <div className={`h4 mb-45 slide3-anim1-1`}>{data.text[4]}</div>
              </div>
              </Waypoint>
              <Waypoint onEnter={(props) => {this.enterAll(2)}} onLeave={(props) => {this.leaveAll(2)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 mb-1 text-danger slide3-anim2`}>{data.text[5]}</div>
              <div className={`h4 mb-45 slide3-anim2-1`}>{data.text[6]}</div>
              </div>
              </Waypoint>
              <Waypoint onEnter={(props) => {this.enterAll(3)}} onLeave={(props) => {this.leaveAll(3)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 mb-1 text-danger slide3-anim3`}>{data.text[7]}</div>
              <div className={`h4 mb-45 slide3-anim3-1`}>{data.text[8]}</div>
              </div>
              </Waypoint>
              <Waypoint onEnter={(props) => {this.enterAll(4)}} onLeave={(props) => {this.leaveAll(4)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`h3 mb-1 text-danger slide3-anim4`}>{data.text[9]}</div>
              <div className={`h4 big-margin slide3-anim4-1`}>{data.text[10]}</div>
              </div>
              </Waypoint>
            </div>
            <div className={`col-12 col-sm-4 col-lg-3 p-45 pt-0`}>
              <div className={`bold-line mb-45 d-none d-lg-block`} />
              <div className={`h6 manrope-bold text-primary mb-45`}>{data.info[0]}</div>
              <div className={`h6 manrope-bold text-primary mb-45 mb-xs-only-0`}>{data.info[1]}</div>
            </div>
          </div>


          {/*

          <div className={`row`}>
            <div className={`col-xs-12 col-sm-2 p-45 pt-xs-only-0 answer-col no-top-border d-none d-sm-block`}>
            </div>
            <div className={`col-xs-12 col-sm-10 p-45 pb-0 main-col no-top-border big-margin`}>
              <div className={`fact position-relative`}>
                <div className={`fact-dot`} />
                <div className={`h3 text-primary ml-45`}>
                  <div dangerouslySetInnerHTML={{__html: data.fact }} />
                </div>
              </div>
            </div>
          </div>

        */}

        </section>
      </Waypoint>
    );
  }
}
