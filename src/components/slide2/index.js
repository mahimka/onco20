import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { DrawSVGPlugin } from "../../vendor/DrawSVGPlugin.js";
import { EasePack } from 'gsap/all'

import Ill from '../../assets/ill0.svg';
import Icon0 from '../../assets/icon0.svg';
import Icon1 from '../../assets/icon1.svg';
import Icon2 from '../../assets/icon2.svg';
import Icon3 from '../../assets/icon3.svg';

import { connect, post, event } from '../../vendor/gtm.js'

export default class Slide2 extends React.Component {
  constructor(props) {
    super(props);
    gsap.registerPlugin(DrawSVGPlugin);
    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    this.els1 = [
      document.querySelector(`.slide2-anim0`),
      document.querySelector(`.slide2-anim2`),
      document.querySelector(`.slide2-anim4`),
      document.querySelector(`.slide2-anim6`)
    ]
    this.els0 = [
      document.querySelector(`.slide2-anim1`),
      document.querySelector(`.slide2-anim3`),
      document.querySelector(`.slide2-anim5`),
      document.querySelector(`.slide2-anim7`)
    ]
    this.els = [
      document.querySelector(`.slide2-anim9`),
      document.querySelector(`.slide2-anim10`),
      document.querySelector(`.slide2-anim11`),
    ]
    this.ill = document.querySelectorAll(`.slide2-anim8 > path`)
    gsap.set(this.els0, {opacity:0, x:10});
    gsap.set(this.els1, {opacity:0, scale:0.5});
    gsap.set(this.els[0], {opacity:0, x:-10});
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[2], {opacity:0, x:10});
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});
    
    this.horline = document.querySelector(`.hor-line2-anim`)
    gsap.set(this.horline, {scaleX:0, transformOrigin: `0% 50%`});
  }

  enterLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.set(this.horline, {opacity:0, scaleX:0, transformOrigin: `0% 50%`});
    gsap.to(this.horline, {opacity: 1, scale:1, ease: "power2.out", duration:1})
  }
  leaveLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.to(this.horline, {opacity: 0, ease: "power2.in", duration:0.5})
  }





  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-3'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }


  enter0(props){
    gsap.killTweensOf([this.els0[0], this.els1[0]]);
    gsap.set(this.els0[0], {opacity:0, x:10});
    gsap.set(this.els1[0], {opacity:0, scale:0.5, transformOrigin: `50% 50%`});
    gsap.to(this.els0[0], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els1[0], {opacity: 1, scale:1, ease: "back.out(2)", duration:1})
  }
  leave0(props){
    gsap.killTweensOf([this.els0[0], this.els1[0]]);
    gsap.to(this.els0[0], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els1[0], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter1(props){
    gsap.killTweensOf([this.els0[1], this.els1[1]]);
    gsap.set(this.els0[1], {opacity:0, x:10});
    gsap.set(this.els1[1], {opacity:0, scale:0.5, transformOrigin: `50% 50%`});
    gsap.to(this.els0[1], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els1[1], {opacity: 1, scale:1, ease: "back.out(2)", duration:1})
  }
  leave1(props){
    gsap.killTweensOf([this.els0[1], this.els1[1]]);
    gsap.to(this.els0[1], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els1[1], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter2(props){
    gsap.killTweensOf([this.els0[2], this.els1[2]]);
    gsap.set(this.els0[2], {opacity:0, x:10});
    gsap.set(this.els1[2], {opacity:0, scale:0.5, transformOrigin: `50% 50%`});
    gsap.to(this.els0[2], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els1[2], {opacity: 1, scale:1, ease: "back.out(2)", duration:1})
  }
  leave2(props){
    gsap.killTweensOf([this.els0[2], this.els1[2]]);
    gsap.to(this.els0[2], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els1[2], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter3(props){
    gsap.killTweensOf([this.els0[3], this.els1[3]]);
    gsap.set(this.els0[3], {opacity:0, x:10});
    gsap.set(this.els1[3], {opacity:0, scale:0.5, transformOrigin: `50% 50%`});
    gsap.to(this.els0[3], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els1[3], {opacity: 1, scale:1, ease: "back.out(2)", duration:1})
  }
  leave3(props){
    gsap.killTweensOf([this.els0[3], this.els1[3]]);
    gsap.to(this.els0[3], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els1[3], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter4(props){
    gsap.killTweensOf([this.ill]);
    gsap.to(this.ill, {opacity: 1, drawSVG:`0% 100%`, ease: "power2.out", duration:1, stagger: 0.025})
  }
  leave4(props){
    gsap.killTweensOf([this.ill]);
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});
  }

  enter5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[0], {opacity:0, x:-10, transformOrigin: `50% 50%`});
    gsap.to(this.els[1], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els[0], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.to(this.els[1], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els[0], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.set(this.els[2], {opacity:0, x:10});
    gsap.to(this.els[2], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.to(this.els[2], {opacity: 0, ease: "power2.in", duration:0.5})
  }








  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section`} data-id={`2`}>

          <div className={`row`}>
            <Waypoint onEnter={(props) => {this.enter5(props)}} onLeave={(props) => {this.leave5(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
            <div className={`col-12 col-lg-2 p-45 d-flex justify-content-sx-start justify-content-lg-end align-items-end question-col`}>
              <div className={`display-2 manrope-light text-secondary slide2-anim9`}>{`01`}</div>
              <div className={`manrope-light pt-3 text-primary vertical-text slide2-anim10`}>{`вопрос`}</div>
            </div>
            </Waypoint>
            <div className={`col-12 col-lg-10 p-45 pt-xs-only-0 h1 manrope-bold text-uppercase text-primary`}>{data.h1}</div>
          </div>


          <Waypoint onEnter={(props) => {this.enterLine(props)}} onLeave={(props) => {this.leaveLine(props)}} bottomOffset={"10%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div className={`row no-gutters`}>
            <div className={`col-12 col-lg-9`}>
              <div className={`hor-line hor-line2-anim`} />
            </div>
          </div>
          </Waypoint>



          <div className={`row`}>
            <div className={`col-12 col-lg-2 p-45 pt-xs-only-0 d-flex justify-content-end align-items-start answer-col`}>
              <Waypoint onEnter={(props) => {this.enter6(props)}} onLeave={(props) => {this.leave6(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`manrope-light pt-0 pt-sm-3 text-primary vertical-sm-text slide2-anim11`}>{`ответ`}</div>
              </Waypoint>
            </div>
            <div className={`col-12 col-sm-8 col-lg-7 p-45 main-col`}>
              <div className={`h4 mb-45`}>{data.text[0]}</div>
              <div className={`h4 mb-45`}>{data.text[1]}</div>
              
              <Waypoint onEnter={(props) => {this.enter0(props)}} onLeave={(props) => {this.leave0(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`d-flex mb-45 align-items-center`} >
                <Icon0 className={`icon slide2-anim0`} data-id={`0`}/>
                <div className={`h3 text-primary ml-45 slide2-anim1`}>{data.list[0]}</div>
              </div>
              </Waypoint>

              <Waypoint onEnter={(props) => {this.enter1(props)}} onLeave={(props) => {this.leave1(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`d-flex mb-45 align-items-center`} >
                <Icon1 className={`icon slide2-anim2`} data-id={`1`}/>
                <div className={`h3 text-primary ml-45 slide2-anim3`}>{data.list[1]}</div>
              </div>
              </Waypoint>
              
              <Waypoint onEnter={(props) => {this.enter2(props)}} onLeave={(props) => {this.leave2(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`d-flex mb-45 align-items-center`} >
                <Icon2 className={`icon slide2-anim4`} data-id={`2`}/>
                <div className={`h3 text-primary ml-45 slide2-anim5`}>{data.list[2]}</div>
              </div>
              </Waypoint>

              <Waypoint onEnter={(props) => {this.enter3(props)}} onLeave={(props) => {this.leave3(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`d-flex mb-45 align-items-center`} >
                <Icon3 className={`icon slide2-anim6`} data-id={`3`}/>
                <div className={`h3 text-primary ml-45 slide2-anim7`}>{data.list[3]}</div>
              </div>
              </Waypoint>

              <div className={`h4 mb-45 big-margin`}>{data.text[2]}</div>
            </div>
              <Waypoint onEnter={(props) => {this.enter4(props)}} onLeave={(props) => {this.leave4(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`col-12 col-sm-4 col-lg-3 p-45 pt-0`}>
                <div className={`bold-line mb-45 d-none d-lg-block`} />
                <Ill className={`illustration slide2-anim8`} />
              </div>
              </Waypoint>
          </div>

        </section>
      </Waypoint>
    );
  }
}
