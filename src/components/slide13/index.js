import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { DrawSVGPlugin } from "../../vendor/DrawSVGPlugin.js";
import { EasePack } from 'gsap/all'

import Ill from '../../assets/ill4.svg';

import { connect, post, event } from '../../vendor/gtm.js'

export default class Slide12 extends React.Component {
  constructor(props) {
    super(props);
    gsap.registerPlugin(DrawSVGPlugin);
    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    this.els = [
      document.querySelector(`.slide13-anim9`),
      document.querySelector(`.slide13-anim10`),
      document.querySelector(`.slide13-anim11`),
      document.querySelector(`.slide13-anim7`),
      document.querySelector(`.slide13-anim0`),
      document.querySelector(`.slide13-anim1`)
    ]
    this.ill = document.querySelectorAll(`.slide13-anim8 > path`)
    gsap.set(this.els[0], {opacity:0, x:-10});
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[2], {opacity:0, x:10});
    gsap.set(this.els[3], {opacity:0, x:10});
    gsap.set(this.els[4], {opacity:0, scaleX:0, transformOrigin: `50% 50%`});
    gsap.set(this.els[5], {opacity:0, x:10});
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});

    this.horline = document.querySelector(`.hor-line13-anim`)
    gsap.set(this.horline, {scaleX:0, transformOrigin: `0% 50%`});
  }

  enterLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.set(this.horline, {opacity:0, scaleX:0, transformOrigin: `0% 50%`});
    gsap.to(this.horline, {opacity: 1, scale:1, ease: "power2.out", duration:1})
  }
  leaveLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.to(this.horline, {opacity: 0, ease: "power2.in", duration:0.5})
  }


  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-14'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }

  enter0(props){
    gsap.killTweensOf([this.els[4], this.els[5]]);
    gsap.set(this.els[4], {opacity:0, scaleX:0, transformOrigin: `50% 50%`});
    gsap.set(this.els[5], {opacity:0, x:10});
    gsap.to(this.els[4], {opacity: 1, scaleX:1, ease: "power2.out", duration:1})
    gsap.to(this.els[5], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave0(props){
    gsap.killTweensOf([this.els[4], this.els[5]]);
    gsap.to(this.els[4], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els[5], {opacity: 0, ease: "power2.in", duration:0.5})
  }


  enter4(props){
    gsap.killTweensOf([this.ill]);
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});
    gsap.to(this.ill, {opacity: 1, drawSVG:`0% 100%`, ease: "power2.out", duration:1, stagger: 0.025})
  }
  leave4(props){
    gsap.killTweensOf([this.ill]);
    gsap.set(this.ill, {opacity:0, drawSVG:`50% 50%`});
  }

  enter5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[0], {opacity:0, x:-10, transformOrigin: `50% 50%`});
    gsap.to(this.els[1], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els[0], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.to(this.els[1], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els[0], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.set(this.els[2], {opacity:0, x:10});
    gsap.to(this.els[2], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.to(this.els[2], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter7(props){
    gsap.killTweensOf(this.els[3]);
    gsap.set(this.els[3], {opacity:0, x:10});
    gsap.to(this.els[3], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave7(props){
    gsap.killTweensOf(this.els[3]);
    gsap.to(this.els[3], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section`} data-id={`13`}>

          <div className={`row`}>
            <Waypoint onEnter={(props) => {this.enter5(props)}} onLeave={(props) => {this.leave5(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
            <div className={`col-12 col-lg-2 p-45 d-flex justify-content-sx-start justify-content-lg-end align-items-end question-col`}>
              <div className={`display-2 manrope-light text-secondary slide13-anim9`}>{`12`}</div>
              <div className={`manrope-light pt-3 text-primary vertical-text slide13-anim10`}>{`вопрос`}</div>
            </div>
            </Waypoint>
            <div className={`col-12 col-lg-10 p-45 pt-xs-only-0 h1 manrope-bold text-uppercase text-primary`}>{data.h1}</div>
          </div>
          
          <Waypoint onEnter={(props) => {this.enterLine(props)}} onLeave={(props) => {this.leaveLine(props)}} bottomOffset={"10%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div className={`row no-gutters`}>
            <div className={`col-12 col-lg-9`}>
              <div className={`hor-line hor-line13-anim`} />
            </div>
          </div>
          </Waypoint>

          <div className={`row`}>
            <div className={`col-12 col-lg-2 p-45 pt-xs-only-0 d-flex justify-content-end align-items-start answer-col`}>
              <Waypoint onEnter={(props) => {this.enter6(props)}} onLeave={(props) => {this.leave6(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`manrope-light pt-0 pt-sm-3 text-primary vertical-sm-text slide13-anim11`}>{`ответ`}</div>
              </Waypoint>
            </div>
            <div className={`col-12 col-sm-8 col-lg-7 p-45 main-col`}>
              <Waypoint onEnter={(props) => {this.enter7(props)}} onLeave={(props) => {this.leave7(props)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`h3 text-danger mb-1 slide13-anim7`}>{data.text[0]}</div>
              </Waypoint>
              <div className={`h4 mb-45`}>{data.text[1]}</div>
              
              <div className={`h4`}>{data.text[2]}</div>
              <div className={`h4`}>{data.text[3]}</div>
              <div className={`h4 mb-45`}>{data.text[4]}</div>
              <div className={`h4 mb-45 big-margin`}>{data.text[5]}</div>

              <Waypoint onEnter={(props) => {this.enter0(props)}} onLeave={(props) => {this.leave0(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div>
              <div className={`bold-line mb-45 d-none d-sm-block slide13-anim0`} />
              
              <div className={`h4 mb-45 slide13-anim1`}>
                <span>{`Подробную информацию об онкологических заболеваниях, их факторах риска, профилактике, диагностике и лечении ищите на официальном портале Минздрава России об онкологии `}</span>
                <span>
                  <a 
                    href='https://onco-life.ru/'
                    target='_blank'
                    onClick={()=>{
                      event('outer_link', { link: 2 })
                    }}
                  >{`onco-life.ru`}</a></span>
              </div>


              </div>
              </Waypoint>
            </div>
            <Waypoint onEnter={(props) => {this.enter4(props)}} onLeave={(props) => {this.leave4(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
            <div className={`col-12 col-sm-4 col-lg-3 p-45 pt-0`}>
              <div className={`bold-line mb-45 d-none d-lg-block`} />
              <Ill className={`illustration slide13-anim8`} />
            </div>
            </Waypoint>
          </div>

        </section>
      </Waypoint>
    );
  }
}
