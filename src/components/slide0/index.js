import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import Start from '../../assets/start.svg';
import Sphere from '../../assets/sphere.svg';
import Sphere2 from '../../assets/sphere2.svg';
// import Lungs from '../../assets/lungs.svg';

import { connect, post, event } from '../../vendor/gtm.js'

export default class Slide0 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
      let mousearrow = document.querySelector(`#start-arrow`);
      gsap.killTweensOf(mousearrow);
      gsap.set(mousearrow, {y:-3})
      gsap.to(mousearrow, {y: 3, repeat:-1, yoyo: true, ease:`power0.out`, duration: 0.75});

    this.els = [
      document.querySelector(`.slide0-anim0`),
      document.querySelector(`.slide0-anim1`),
    ]
    gsap.set(this.els[0], {opacity:0});
    gsap.set(this.els[1], {opacity:0});


  }

  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-1'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }

  enter0(props){
    gsap.killTweensOf([this.els[0]]);
    gsap.set(this.els[0], {opacity:0});
    gsap.to(this.els[0], {opacity: 1, ease: "power2.out", duration:2})

  }
  leave0(props){
    gsap.killTweensOf([this.els[0]]);
    gsap.to(this.els[0], {opacity: 0, ease: "power2.in", duration:2})
  }

  enter1(props){
    gsap.killTweensOf([this.els[1]]);
    gsap.set(this.els[1], {opacity:0});
    gsap.to(this.els[1], {opacity: 1, ease: "power2.out", duration:2})

  }
  leave1(props){
    gsap.killTweensOf([this.els[1]]);
    gsap.to(this.els[1], {opacity: 0, ease: "power2.in", duration:2})
  }









  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section position-relative`} data-id={`0`}>
          <div className={`lungs`} />
          
          <Waypoint onEnter={(props) => {this.enter0(props)}} onLeave={(props) => {this.leave0(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div>
          <Sphere2 className={`sphere2 slide0-anim0`} />
          </div>
          </Waypoint>
          
          <Waypoint onEnter={(props) => {this.enter1(props)}} onLeave={(props) => {this.leave1(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div>
          <Sphere2 className={`sphere3 slide0-anim1`} />
          </div>
          </Waypoint>


          <div className={`row folder d-flex flex-column-reverse flex-lg-column justify-content-start justify-content-lg-end`}>
              <div className={`row`}>
                <div className={`col-12 pl-sm-0 folder-h1 text-primary manrope-light ml-45 mr-45`} dangerouslySetInnerHTML={{__html: data.h1 }} />
                <div className={`h3 folder-text1 text-danger manrope-bold text-uppercase ml-45 mr-45`}>
                    {data.text}
                </div>
              </div>
              <div className={`row`}>
                <div className={`col-xs-12 col-lg-2 p-45 pt-xs-only-0 answer-col no-top-border d-none d-sm-block`}>
                </div>
                <div className={`col-xs-12 col-lg-9 p-45 pb-0`}>
                  <div className={`folder-line`} />
                  <Start className={`start`} />
                  <Sphere className={`sphere`} />
                  <div className={`h3 folder-text text-danger manrope-bold text-uppercase text-center`}>
                  </div>
                </div>
              </div>
          </div>

        </section>
      </Waypoint>
    );
  }
}


