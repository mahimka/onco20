import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import Icon4 from '../../assets/icon4.svg';
import Icon5 from '../../assets/icon5.svg';
import Icon6 from '../../assets/icon6.svg';
import Triangle from '../../assets/triangle.svg';

import { connect, post, event } from '../../vendor/gtm.js'


export default class Slide8 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    this.els = [
      document.querySelector(`.slide8-anim9`),
      document.querySelector(`.slide8-anim10`),
      document.querySelector(`.slide8-anim11`)
    ]

    this.els0 = [
      document.querySelector(`.slide8-anim0`),
      document.querySelector(`.slide8-anim1`),
      document.querySelector(`.slide8-anim2`),
      document.querySelector(`.slide8-anim3`)
    ]

    gsap.set(this.els0, {opacity:0, x:10});

    gsap.set(this.els[0], {opacity:0, x:-10});
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[2], {opacity:0, x:10});

    this.quote = document.querySelector(`.slide8-anim4`)
    this.autor = document.querySelector(`.slide8-anim5`)
    this.tline = document.querySelector(`.slide8-anim6`)
    this.bline = document.querySelector(`.slide8-anim7`)
    this.angle = document.querySelector(`.slide8-anim8`)
    this.txt = document.querySelector(`.slide8-anim12`)
    gsap.set(this.quote, {opacity:0, scale:3, transformOrigin: `50% 25%`});
    gsap.set(this.autor, {opacity:0, x:10});
    gsap.set(this.tline, {opacity:0, scaleX:0, transformOrigin: `50% 50%`});
    gsap.set(this.bline, {opacity:0, scaleX:0, transformOrigin: `50% 50%`});
    gsap.set(this.angle, {opacity:0});
    gsap.set(this.txt, {opacity:0});

    this.horline = document.querySelector(`.hor-line8-anim`)
    gsap.set(this.horline, {scaleX:0, transformOrigin: `0% 50%`});
  }

  enterLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.set(this.horline, {opacity:0, scaleX:0, transformOrigin: `0% 50%`});
    gsap.to(this.horline, {opacity: 1, scale:1, ease: "power2.out", duration:1})
  }
  leaveLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.to(this.horline, {opacity: 0, ease: "power2.in", duration:0.5})
  }

  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-9'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }


  enter0(props){
    gsap.killTweensOf(this.quote);
    gsap.set(this.quote, {opacity:0, scale:3});
    gsap.set(this.tline, {opacity:0, scaleX:0, transformOrigin: `50% 50%`});
    gsap.set(this.bline, {opacity:0, scaleX:0, transformOrigin: `50% 50%`});
    gsap.set(this.angle, {opacity:0});
    gsap.set(this.txt, {opacity:0});
    gsap.to(this.quote, {opacity: 1, scale:1, ease: "power2.out", duration:1})
    gsap.to(this.tline, {opacity:1, scaleX:1, ease: "power2.out", duration:1});
    gsap.to(this.bline, {opacity:1, scaleX:1, ease: "power2.out", duration:1});
    gsap.to(this.angle, {opacity:1, ease: "power2.out", duration:1});
    gsap.to(this.txt, {opacity:1, ease: "power2.out", duration:1});
  }
  leave0(props){
    gsap.killTweensOf([this.quote]);
    gsap.to(this.quote, {opacity: 0, ease: "power2.out", duration:0.5})
    gsap.to(this.tline, {opacity:0, ease: "power2.out", duration:0.5});
    gsap.to(this.bline, {opacity:0, ease: "power2.out", duration:0.5});
    gsap.to(this.angle, {opacity:0, ease: "power2.out", duration:0.5});
    gsap.to(this.txt, {opacity:0, ease: "power2.out", duration:0.5});
  }
  enter1(props){
    gsap.killTweensOf(this.autor);
    gsap.set(this.autor, {opacity:0, x:-10});
    gsap.to(this.autor, {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave1(props){
    gsap.killTweensOf([this.autor]);
    gsap.to(this.autor, {opacity: 0, ease: "power2.out", duration:0.5})
  }


  enterAll(id){
    gsap.killTweensOf([this.els0[id]]);
    gsap.set(this.els0[id], {opacity:0, x:10});
    gsap.to(this.els0[id], {opacity: 1, x:0, ease: "power2.out", duration:1})

  }
  leaveAll(id){
    gsap.killTweensOf([this.els0[id]]);
    gsap.to(this.els0[id], {opacity: 0, ease: "power2.out", duration:0.5})
  }

  enter5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.set(this.els[1], {opacity:0, x:10});
    gsap.set(this.els[0], {opacity:0, x:-10, transformOrigin: `50% 50%`});
    gsap.to(this.els[1], {opacity: 1, x:0, ease: "power2.out", duration:1})
    gsap.to(this.els[0], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave5(props){
    gsap.killTweensOf([this.els[0], this.els[1]]);
    gsap.to(this.els[1], {opacity: 0, ease: "power2.in", duration:0.5})
    gsap.to(this.els[0], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  enter6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.set(this.els[2], {opacity:0, x:10});
    gsap.to(this.els[2], {opacity: 1, x:0, ease: "power2.out", duration:1})
  }
  leave6(props){
    gsap.killTweensOf(this.els[2]);
    gsap.to(this.els[2], {opacity: 0, ease: "power2.in", duration:0.5})
  }

  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section`} data-id={`8`}>

          <div className={`row`}>
            <Waypoint onEnter={(props) => {this.enter5(props)}} onLeave={(props) => {this.leave5(props)}} bottomOffset={"30%"} scrollableAncestor={window} fireOnRapidScroll={false}>
            <div className={`col-12 col-lg-2 p-45 d-flex justify-content-sx-start justify-content-lg-end align-items-end question-col`}>
              <div className={`display-2 manrope-light text-secondary slide8-anim9`}>{`07`}</div>
              <div className={`manrope-light pt-3 text-primary vertical-text slide8-anim10`}>{`вопрос`}</div>
            </div>
            </Waypoint>
            <div className={`col-12 col-lg-10 p-45 pt-xs-only-0 h1 manrope-bold text-uppercase text-primary`}>{data.h1}</div>
          </div>


          <Waypoint onEnter={(props) => {this.enterLine(props)}} onLeave={(props) => {this.leaveLine(props)}} bottomOffset={"10%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div className={`row no-gutters`}>
            <div className={`col-12 col-lg-9`}>
              <div className={`hor-line hor-line8-anim`} />
            </div>
          </div>
          </Waypoint>

          <div className={`row`}>
            <div className={`col-12 col-lg-2 p-45 pt-xs-only-0 d-flex justify-content-end align-items-start answer-col`}>
              <Waypoint onEnter={(props) => {this.enter6(props)}} onLeave={(props) => {this.leave6(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`manrope-light pt-0 pt-sm-3 text-primary vertical-sm-text slide8-anim11`}>{`ответ`}</div>
              </Waypoint>
            </div>
            <div className={`col-12 col-sm-8 col-lg-7 p-45 main-col`}>
              <div className={`h4 mb-45`}>{data.text[0]}</div>


              <Waypoint onEnter={(props) => {this.enter0(props)}} onLeave={(props) => {this.leave0(props)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`quotes p-45 position-relative`}>
                <div className={`quotes-top-line slide8-anim6`} />
                <div className={`quotes-bottom-line slide8-anim7`} />
                <div className={`quote position-absolute text-primary slide8-anim4`}>{`“`}</div>
                <div className={`h4 ml-45 slide8-anim12`}>{data.quote.text}</div>
                <div className={`triangle-line`} />
                <Triangle className={`triangle slide8-anim8`} />
              </div>
              </Waypoint>

              <Waypoint onEnter={(props) => {this.enter1(props)}} onLeave={(props) => {this.leave1(props)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
              <div className={`p-45 big-margin`}>
                <div className={`h6 ml-45 mr-45 manrope-bold text-right slide8-anim5`}>{data.quote.autor}</div>
              </div>
              </Waypoint>



            </div>
            

            <div className={`col-12 col-sm-4 col-lg-3 p-45 pt-0`}>
              <div className={`bold-line mb-45 d-none d-lg-block`} />
              <div className={`h6 text-primary manrope-bold mb-45`}>{data.info[0]}</div>
              <div className={`d-flex flex-sm-column align-items-center align-items-sm-start mb-45`}>
                <Waypoint onEnter={(props) => {this.enterAll(0)}} onLeave={(props) => {this.leaveAll(0)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
                <div className={`h6 slide8-anim0`}>{data.info[1]}</div>
                </Waypoint>
              </div>
              <div className={`d-flex flex-sm-column align-items-center align-items-sm-start mb-45`}>
                <Waypoint onEnter={(props) => {this.enterAll(1)}} onLeave={(props) => {this.leaveAll(1)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
                <div className={`h6 slide8-anim1`}>{data.info[2]}</div>
                </Waypoint>
              </div>
              <div className={`d-flex flex-sm-column align-items-center align-items-sm-start mb-45`}>
                <Waypoint onEnter={(props) => {this.enterAll(2)}} onLeave={(props) => {this.leaveAll(2)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
                <div className={`h6 slide8-anim2`}>{data.info[3]}</div>
                </Waypoint>
              </div>
              <div className={`d-flex flex-sm-column align-items-center align-items-sm-start mb-45`}>
                <Waypoint onEnter={(props) => {this.enterAll(3)}} onLeave={(props) => {this.leaveAll(3)}} bottomOffset={"25%"} scrollableAncestor={window} fireOnRapidScroll={false}>
                <div className={`h6 slide8-anim3`}>{data.info[4]}</div>
                </Waypoint>
              </div>

              <div className={`bold-line mb-45 d-none d-sm-block`} />
              <div className={`h6 mb-45`}>{data.info[5]}</div>
              <div className={`h6 mb-45`}>{data.info[6]}</div>
              <div className={`h6 mb-45`}>{data.info[7]}</div>

            </div>





          </div>

        </section>
      </Waypoint>
    );
  }
}
