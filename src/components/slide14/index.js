import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import Logo from '../../assets/logo.svg';
import Logo2 from '../../assets/logo2.svg';

import { connect, post, event } from '../../vendor/gtm.js'

export default class Slide13 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    this.horline = document.querySelector(`.hor-line14-anim`)
    gsap.set(this.horline, {scaleX:0, transformOrigin: `0% 50%`});
  }

  enterLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.set(this.horline, {opacity:0, scaleX:0, transformOrigin: `0% 50%`});
    gsap.to(this.horline, {opacity: 1, scale:1, ease: "power2.out", duration:1})
  }
  leaveLine(props){
    gsap.killTweensOf([this.horline]);
    gsap.to(this.horline, {opacity: 0, ease: "power2.in", duration:0.5})
  }

  handleWaypointEnter(props){
    if(!this.state.fired){
      event('anchor', {anchor: 'anchor-15'});
      this.setState({fired: true});
    }
  }
  handleWaypointLeave(props){

  }

  render() {
    const data = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ctr-section`} data-id={`14`}>
          
          <Waypoint onEnter={(props) => {this.enterLine(props)}} onLeave={(props) => {this.leaveLine(props)}} bottomOffset={"20%"} scrollableAncestor={window} fireOnRapidScroll={false}>
          <div className={`row no-gutters`}>
            <div className={`col-12`}>
              <div className={`hor-line hor-line14-anim`} />
            </div>
          </div>
          </Waypoint>

          <div className={`row`}>
            <div className={`col-12 col-lg-2 p-45 pt-xs-only-0 d-flex justify-content-end align-items-start answer-col`}>
            </div>
            <div className={`col-12 col-lg-10 p-45 main-col`}>
              <div className={`row`}>
                <div className={`col-xs-12 col-sm-3 mb-45`}>
                  <Logo2 className={`mb-45 logo2`} />
                  <Logo className={`mb-45 logo`} />
                  <div className={`h6 manrope-bold mb-45 mb-xs-only-0`}>{data.copy}</div>
                  <div className={`h6`}>{data.year}</div>
                </div>
                <div className={`col-xs-12 col-sm-9 pr-45`}>
                  <div className={`h6`}>{data.text[0]}</div>
                  <div className={`h6 mb-45`}>{data.text[1]}</div>
                  <div className={`h6 mb-45`}>{data.text[2]}</div>
                  <div className={`h6 mb-45`}>{data.text[3]}</div>
                  <div className={`h6 mb-45`}>{data.text[4]}</div>
                  <div className={`h6 mb-45`}>{data.text[5]}</div>
                </div>
              </div>
            </div>
          </div>

        </section>
      </Waypoint>
    );
  }
}
