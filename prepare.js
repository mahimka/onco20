const fs = require('fs');
const { JSDOM } = require("jsdom");
let cssArr = [], jsArr = []; 
let cfg = require('./package.json')

const projectName = cfg.name

console.log('>> will create dist/inject.js')

JSDOM.fromFile('dist/index.html').then(dom => {
  let scriptList = dom.window.document.getElementsByTagName('script')
  let linkList = dom.window.document.getElementsByTagName('link')

  for (let item of scriptList) {
    jsArr.push(item.getAttribute('src'));
  }
  console.log('----js----\n', jsArr)

  for (let item of linkList) {
    // если добавить в public/index.html ссылку на google font, он добавится в inject.js
    if(/app\.(.*)\.css|chunk-vendors\.(.*)\.css|fonts\.googleapis/ig.test(item.outerHTML)) {
      cssArr.push(item.getAttribute('href'));
    }
  }
  console.log('----css----\n', cssArr)
  
  jsArr = [...new Set(jsArr)];
  cssArr = [...new Set(cssArr)];

  tpl = tpl.replace('###ARRJS###', JSON.stringify(jsArr))
  tpl = tpl.replace('###ARRCSS###', JSON.stringify(cssArr))
  tpl = tpl.replace(/###PROJECT_NAME###/g, projectName)

  fs.writeFileSync('dist/inject.js', tpl)
});


let tpl = `
( function () {
  console.log('>> inject');
  function createEl (url) {
    var el = document.createElement('script');
    el.type= 'text/javascript';
    el.src= url
    return el
  }
  function createCSS (url) {
    var el = document.createElement('link');
    el.rel= 'stylesheet'
    el.href= url
    return el
  }
  var arrJS = ###ARRJS###;
  var arrCSS = ###ARRCSS###;
  var div = document.createElement('div');
  div.id = '###PROJECT_NAME###';

  var script = null;
  var docScripts = document.scripts;
  for(var i=0;i<docScripts.length;i++) {
    if(docScripts[i].src.indexOf('###PROJECT_NAME###') > -1) {
      script = docScripts[i]
    }
  }

  if(script) {
    var dataUid = script.getAttribute('data-uid');
    var rootEl = document.querySelector('div[data-uid="'+dataUid+'"]')
    console.log('>> rootEl ', rootEl)
    rootEl.appendChild(div);

    for(var i=0;i<arrJS.length;i++) {
      rootEl.appendChild(createEl(arrJS[i]));
    }
    for(var i=0;i<arrCSS.length;i++) {
      rootEl.appendChild(createCSS(arrCSS[i]));
    }
  }
} )()
`;